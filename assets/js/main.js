/*===============================================

Theme Name:ProperBusinessSolutio HTML Template
Version:1.0
Author: ITCLAN
Support: itclan@gmail.com
Description: ProperBusinessSolutio HTML Template

NOTE:
=====
Please DO NOT EDIT THIS JS, you may need to use "custom.js".

===============================================**/


(function($) {
    "use strict";


    /*========================================
        Preloader
    ========================================*/
    //
    //    $(window).on('load', function () {
    //        $("#loading").fadeOut(500);
    //
    //    });


    $(window).on('load', function () {
        $("#loading").fadeOut(2000);
    
    });

    $('.banner-slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: false,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 1500,
    });

    $('.clients-logo').slick({
    infinite: true,
    // speed: 250,
    slidesToShow: 6,
    slidesToScroll: 1,
    centerMode: false,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 0,
    speed: 2000,
    pauseOnHover: false,
    cssEase: 'linear',
    infinite: true,
    responsive: [
        {
        breakpoint: 992,
        settings: {
            slidesToShow: 3,
        }
        },
        
        {
        breakpoint: 768,
        settings: {
            slidesToShow: 2,
        }
        },
        {
        breakpoint: 500,
        settings: {
            slidesToShow: 1,
            arrows: false,
        }
        },
    ]

    });


    /*========================================
        Scroll  top
    ========================================*/

    var scrollTop = $('.ic-scroll-top');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
         scrollTop.show();
            scrollTop.css({
                'bottom': '12%',
                'opacity': '1',
                'transition': 'all .5s ease-in-out'
            });
        } else {
            scrollTop.css({
                'bottom': '-15%',
                'opacity': '0',
                'transition': 'all .5s ease-in-out'
            })
        }
    });
    scrollTop.on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
        return false;
    });

// ========================================
// ======= Ic Mobile menu activation ======
// ========================================

$('.ic-mobile-menu-open,.ic-mobile-menu-overlay').on('click', function () {
    $('.ic-mobile-menu-wrapper,.ic-mobile-menu-overlay').addClass('active')
});
$('.ic-menu-close,.ic-mobile-menu-overlay').on('click', function () {
    $('.ic-mobile-menu-wrapper,.ic-mobile-menu-overlay').removeClass('active')
});

// ========================================
// ======= Ic Mobile Menu Toggle ==========
// ========================================

var $offcanvasNav = $('.ic-mobile-menu'),
$offcanvasNavSubMenu = $offcanvasNav.find('.sub-menu');
 $offcanvasNavSubMenu.parent().prepend('<span class="menu-expand"><i class="icofont-simple-down"></i></span>');
 
 $offcanvasNavSubMenu.slideUp();
 
 $offcanvasNav.on('click', 'li a, li .menu-expand', function(e) {
    var $this = $(this);
    if ( ($this.parent().attr('class').match(/\b(menu-item-has-children|has-children|has-sub-menu)\b/)) && ($this.attr('href') === '#' || $this.hasClass('menu-expand')) ) {
        e.preventDefault();
        if ($this.siblings('ul:visible').length){
            $this.siblings('ul').slideUp('slow');
        }else {
            $this.closest('li').siblings('li').find('ul:visible').slideUp('slow');
            $this.siblings('ul').slideDown('slow');
        }
    }
    if( $this.is('a') || $this.is('span') || $this.attr('clas').match(/\b(menu-expand)\b/) ){
        $this.parent().toggleClass('menu-open');
    }else if( $this.is('li') && $this.attr('class').match(/\b('menu-item-has-children')\b/) ){
        $this.toggleClass('menu-open');
    }
 });

 

   






})(jQuery);